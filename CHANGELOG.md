# Changelog

In this file all important changes to `cadix/superoffice-api` are listed.

## 0.4.2
- Converted Guzzle requests to Laravel Http. There for requests can now be mocked;
- Requests are limited by config (#1);
- Improved autocomplete; 
- Added street address of a contact;
- Added default methods to contact and person. These methods should create model with some default values;
- Changed validators for models. There is now also a validator method;
- Improved paginator;
- Validation rules now use model facades;
- Added currency;
- Improved CI/CD

## 0.4.1
- Improved return types for find, all and get methods
- Added multiple languages for some countries. Otherwise, it could not return the right ISO 2 code. For example if country is returned in Dutch as Nederland instead of Netherlands it would return null for domain_name;

## 0.4.0
- Dropped PHP 7.4 support;
- Improved types;

## 0.3.5
- Added client as facade
