<?php

return [
    'accept' => env('SUPEROFFICE_ACCEPT', 'application/json'),

    /*
     * The domain and customer_id together create the url
     * The domain depends on the stage
     * https://community.superoffice.com/en/developer/create-apps/overview/application-environments/
     *
     * Development: sod
     * Stage: qaonline
     * Production: online
     */
    'environment' => env('SUPEROFFICE_ENVIRONMENT'),
    'customer_id' => env('SUPEROFFICE_CUSTOMER_ID'),

    'client_id'     => env('SUPEROFFICE_CLIENT_ID'),
    'client_secret' => env('SUPEROFFICE_CLIENT_SECRET'),

    'private_key' => env('SUPEROFFICE_PRIVATE_KEY', storage_path('private-key.pem')),

    /*
     * The URL that is used for SuperOffice to redirect to after authenticating
     */
    'redirect_url' => env('SUPEROFFICE_REDIRECT_URL'),

    /*
     * The type of authorization
     * Could be any of the supported types by SuperOffice
     * basic, ticket, bearer
     * https://community.superoffice.com/documentation/sdk/SO.NetServer.Web.Services/html/Reference-WebAPI-Authentication-Authentication.htm
     */
    'authorization' => env('SUPEROFFICE_AUTHORIZATION', 'basic'),

    'retries' => env('SUPEROFFICE_RETRIES', 3),
    'timeout' => env('SUPEROFFICE_TIMEOUT', 2), // in seconds
];
