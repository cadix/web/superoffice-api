![banner](https://banners.beyondco.de/SuperOffice%20API.png?theme=light&packageName=cadix%2Fsuperoffice-api&pattern=cage&style=style_1&description=A+Laravel+wrapper+package+for+SuperOffice+API&md=1&showWatermark=1&fontSize=200px&images=link)

# Introduction

This package is ment for OAuth actions with SuperOffice's REST API.

[![coverage report](https://gitlab.com/cadix/superoffice-api/badges/master/coverage.svg?style=flat)](https://gitlab.com/cadix/superoffice-api/-/commits/master)
[![pipeline status](https://gitlab.com/cadix/superoffice-api/badges/master/pipeline.svg?style=flat)](https://gitlab.com/cadix/superoffice-api/-/commits/master)

[[_TOC_]]

# Installation

To install the package run the following command:

```bash
composer require cadix/superoffice-api
```

Then run migrations:
```bash
php artisan vendor:publish --provider="Cadix\SuperOfficeApi\SuperOfficeApiServiceProvider" --tag="migrations"
```

You could also use the command `php artisan superoffice-api:install` which publishes both config and migration files.

You can give the `HasSuperOfficeOAuth` trait to the model that is related to a SuperOffice user. 
This will store the required tokens for that model to authorize any other API requests.

## SuperOffice
After installing the package, you still need to register the app with SuperOffice. 
To do this go [here](https://community.superoffice.com/en/developer/create-apps/resources/developer-registration/) and take the following steps in the menu.

### Get your customer_id, client_id and client_secret

First you need to register as a developer [here](https://community.superoffice.com/en/developer/create-apps/resources/developer-registration/) if you haven't already done so.
It will take a bit time before getting a response.

You should now be able to login and find your `customer_id`. The `customer_id` can be found under `Help > About`.

![about](./images/about.png)

The next step is to register the app you're making [here](https://community.superoffice.com/en/developer/create-apps/resources/application-registration/).
Make sure that the checkbox `Server to server with system user` is checked.

![service-to-service checkbox](./images/service-to-service.PNG)

After submitting the form, it could take a day before getting a response. You should follow the link provided in the response mail which is where you find the `client_id` and `client_secret`

Create or edit a `web.config` file as explained [here](https://community.superoffice.com/documentation/sdk/SO.NetServer.Web.Services/html/Reference-WebAPI-Authentication-Authentication.htm).

```xml
<WebApi>
      <add key="AuthorizeWithUsername" value="false" />
      <add key="AuthorizeWithTicket" value="true" />
      <add key="AuthorizeWithImplicit" value="false" />
      <add key="CORSEnable" value="true" />
      <add key="CORSOrigin" value="https://mail.google.com https://dev.mail.google.com" />
</WebApi>
``` 

### Start application certification
When asked to list used endpoints you can use the following list:
    
- Contact
- Document
- List/Business
- List/Category
- List/Country
- Person
- User

# Usage

## Authentication

At this point in time the package doesn't fully support authenticating with OAuth. 
It does allow for usage with the [SuperOffice Socialite Provider](https://socialiteproviders.com/SuperOffice/).
The `.env` settings are the same as well. 

When using the socialite provider, the callback function could look like the following:
```php
public function superofficeCallback(): RedirectResponse
{
    $user = Socialite::driver('superoffice')->stateless()->user();

    // Validate the provided JWT
    (new \Cadix\SuperOfficeApi\Validators\JWTValidator)->validateAndVerifyJwt($user->accessTokenResponseBody['id_token']);

    (new \Cadix\SuperOfficeApi\Session())->setTokens(auth()->user(),  $user->token, $user->refreshToken, $user->id);

    return;
}
```

## Models

| Model | Create | Read | Update | Delete | Usage |
| :---- | :----: | :--: | :----: | :----: | :---- |
| Business |     | X    |        |        | (new Business) |
| Category |     | X    |        |        | (new Category) |
| Contact | X    | X    | X      | X      | (new Contact)  |
| Country |      | X    |        |        | (new Country)  |
| Document |     | X    |        |        | (new Document) |
| Person | X     | X    | X      | X      | (new Person)   |
| User   |       | X    |        |        | (new User)     |

### Methods

| Method | Result |
| :----- | :----- |
| `all()`  | Get all of a model |
| `get()`  | Get all of a model but allows for filtering |
| `create($attributes = [])` | Create a new model |
| `default()` | Create a new model with some default values |
| `update($id, $attributes = [])` | Update existing model |
| `delete($id)` | Delete model |
| `select($columns = [])` | Specify fields to select |
| `filter($filters = [])` | Add [filters](#filters) |
| `orderBy(['column' => 'direction'])` | Order the results |
| `entities()` | |
| `top(10)` | limit the results |
| `limit(10)` | alias for top |
| `skip(10)` | skip X amount of results |
| `mode()` | |
| `options()` | |
| `contexts()` | |
| `format()` | Override `accept` header |
| `jsonSafe()` | |
| `output()` | |
| `post()` | |
| `put()` | |

## Filters
To help with adding filters it is possible to use the provided filters. The filter can be accessed with `(new Filter)`.
All filters have the same syntax `like($column, $value, $boolean)`. Where `$value` can be an `int`, `string` or `array`. 
`$boolean` is for `and` or `or` by default `and`. 

| Method | Result |
| :----- | :---- |
| `like()` | alias for many other filters |
| `begins()` | string starts with |
| `contains()` | string contains |
| `is()` | string equals |
| `bool()` | boolean columns |
| `equals()` | int equals |
| `gt()` | int greater than |
| `ge()` | int greater or equals |
| `greaterThanOrEquals()`  | `ge` alias |
| `greaterThan()` | `gt` | alias |
| `lt()` | int less than |
| `le()` | int less than or equals |
| `lessThanOrEquals()` | `le` alias |
| `lessThan()` | `lt` alias |
| `whereIn()` | |
| `whereNotIn()` | |

If you want to make a sub select do the following:
```php
$filter = (new Filter);

$filter->filters[] = '(';
$filter->contains('name', $search, 'or')
        ->contains('deparment', $search, 'or');
$filter->filters[] = ')';
```

This behaviour is still subject to change.

## Validation rules
The provided validations by SuperOffice feel quite limited. There for this package has some additional validation rules.

### Unique
The `unique` rule will check if the `column` and `value` combination is unique in a given model.
    
For example the following will check if the relation number is unique in contacts/ companies. Except for a specific `$id`.     
```php
return [
    'relation_number' => [
        'required',
        'int',
        new SuperOfficeUnique(Contact::class, 'Number2', $id)
    ]
]
```

### Exists
The `exists` rule will check if the `column` and `value` exists in a given model.

# Configuration

Want to change the default configuration use the following command:

    php artisan vendor:publish --provider="Cadix\SuperOfficeApi\SuperOfficeApiServiceProvider" --tag="config"

## Options

- `SUPEROFFICE_ENVIRONMENT` this depends on the stage of approval your app is. Can be `sod`, `qaonline` or `online`
- `SUPEROFFICE_CUSTOMER_ID` 
- `SUPEROFFICE_REDIRECT_URL` 
- `SUPEROFFICE_CLIENT_ID`
- `SUPEROFFICE_PRIVATE_KEY` defaults to `storage_path('private-key.pma')`, this is the same location as oauth keys would be stored
- `SUPEROFFICE_RETRIES` defaults to 3. Sets the maximum amount of retries if a request fails;
- `SUPEROFFICE_TIMEOUT` default to 2 seconds. Sets the amount at which a request times out;

# TODO
Please see the [issues list](https://gitlab.com/cadix/superoffice-api/-/issues).

# About Cadix
With a team of passionate professionals [Cadix](https://www.cadix.nl) delivers solutions for designing, managing and sharing of information in construction, mechanical engineering and infra.
We realize this with our trainings, consultancy and secondment. 
With the extensive knowledge of our professionals we are able to find and realize a suitable solution for you.  

# Notes
## Person
- When creating a person, by default the person needs to be associated with a contact. 
  This can be turned off in `Preferences > Global preferences > Mandatory company on contact`; 

# References
- https://docs.superoffice.com/api/reference/restful/rest/ (docs)
- https://community.superoffice.com/documentation/sdk/SO.NetServer.Web.Services/html/Reference-WebAPI-REST-REST.htm (docs)
- https://github.com/SuperOfficeDocs/superoffice-docs
- https://github.com/SuperOffice/devnet-php-oidc-soap
- https://github.com/lcobucci/jwt
- https://github.com/roydejong/superoffice-webapi-php-sdk/blob/master/src/Security/JwtValidator.php
