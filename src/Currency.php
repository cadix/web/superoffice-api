<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\Currency as CurrencyMap;

class Currency extends Model
{
    protected string $model = 'List/Currency/Items';

    /**
     * @return array<CurrencyMap>|null
     */
    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();

        $currencies = null;
        foreach ($response as $currency) {
            $currencies[] = new CurrencyMap($currency);
        }

        return $currencies;
    }

    /**
     * @return array<CurrencyMap>|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::get();

        $currencies = null;
        foreach ($response as $currency) {
            $currencies[] = new CurrencyMap($currency);
        }

        return $currencies;
    }

    /**
     * @param int $id
     *
     * @return CurrencyMap|null
     */
    public function find(int $id): CurrencyMap|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return new CurrencyMap(parent::get());
    }
}
