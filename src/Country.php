<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\Country as CountryMap;

class Country extends Model
{
    protected string $model = 'List/Country/Items';

    /**
     * @return array<CountryMap>|null
     */
    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();

        $countries = null;
        foreach ($response as $country) {
            $countries[] = new countryMap($country);
        }

        return $countries;
    }

    /**
     * @return array<CountryMap>|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::get();

        $countries = null;
        foreach ($response as $country) {
            $countries[] = new countryMap($country);
        }

        return $countries;
    }

    public function find(int $id): CountryMap|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return new CountryMap(parent::get());
    }
}
