<?php

namespace Cadix\SuperOfficeApi;

class Filter
{
    public string $filter = '';
    public array $filters = [];

    public function like(string $column, string $value, string $boolean = 'and'): self
    {
        $length = mb_strlen($value);
        $found = str_contains($value, '%');
        $firstOccurrence = strpos($value, '%');
        $lastOccurrence = strpos($value, '%', 1);

        $value = str_replace('%', '', $value);

        if ($found && false !== $firstOccurrence && 0 === $firstOccurrence && false !== $lastOccurrence && $lastOccurrence === ($length - 1)) {
            return $this->contains($column, $value, $boolean);
        }

        if ($found && false !== $lastOccurrence && $lastOccurrence === ($length - 1)) {
            return $this->begins($column, $value, $boolean);
        }

        if ($found && false !== $firstOccurrence && 0 === $firstOccurrence) {
            return $this->ends($column, $value, $boolean);
        }

        return $this->is($column, $value, $boolean);
    }

    public function begins(string $column, string $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            "%s begins '%s'",
            $column,
            $value
        ), $boolean);
    }

    public function ends(string $column, string $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            "%s ends '%s'",
            $column,
            $value
        ), $boolean);
    }

    public function contains(string $column, string $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            "%s contains '%s'",
            $column,
            $value
        ), $boolean);
    }

    public function is(string $column, string $value, string $boolean = 'and'): self
    {
        if (is_numeric($value)) {
            return $this->equals($column, $value, $boolean);
        }

        return $this->add(sprintf(
            "%s is '%s'",
            $column,
            $value
        ), $boolean);
    }

    public function bool(string $column, bool $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            '%s = %b',
            $column,
            $value
        ), $boolean);
    }

    public function get(): string
    {
        return implode('', $this->filters);
    }

    public function equals(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            '%s equals %s',
            $column,
            $value
        ), $boolean);
    }

    public function gt(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            '%s gt %s',
            $column,
            $value
        ), $boolean);
    }

    public function ge(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            '%s ge %s',
            $column,
            $value
        ), $boolean);
    }

    public function greaterThanOrEquals(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->ge($column, $value, $boolean);
    }

    public function greaterThan(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->gt($column, $value, $boolean);
    }

    public function lt(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            '%s lt %s',
            $column,
            $value
        ), $boolean);
    }

    public function le(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->add(sprintf(
            '%s le %s',
            $column,
            $value
        ), $boolean);
    }

    public function lessThanOrEquals(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->le($column, $value, $boolean);
    }

    public function lessThan(string $column, int $value, string $boolean = 'and'): self
    {
        return $this->lt($column, $value, $boolean);
    }

    /**
     * @param int[]|string[] $in
     *
     * @return $this
     *
     * https://community.superoffice.com/en/developer/forum/rooms/topic/netserver-api-group/web-services/operators-for-int-fields-require-a-fieldinfo-argument-of-type-fkarray-got-appointmenttaskidx-fk/
     */
    public function whereIn(string $column, array $in, string $boolean = 'and'): self
    {
        foreach ($in as $key => $value) {
            if (! is_string($value)) {
                continue;
            }

            $in[$key] = "'{$value}'";
        }

        return $this->add(sprintf(
            '%s in(%s)',
            $column,
            implode(',', $in)
        ), $boolean);
    }

    /**
     * @param int[]|string[] $notIn
     *
     * @return $this
     */
    public function whereNotIn(string $column, array $notIn, string $boolean = 'and'): self
    {
        foreach ($notIn as $key => $value) {
            if (! is_string($value)) {
                continue;
            }

            $notIn[$key] = "'{$value}'";
        }

        return $this->add(sprintf(
            '%s notOneOf(%s)',
            $column,
            implode(',', $notIn)
        ), $boolean);
    }

    public function where(string $column,  string|int $value, string $operator = '=', string $boolean = 'and'): self
    {
        $method = null;
        switch ($operator) {
            case '=':
                if (is_string($value)) {
                    $method = 'is';
                }

                if (is_numeric($value)) {
                    $method = 'equals';
                }

                break;
            case '>':
                $method = 'gt';

                break;
            case '>=':
                $method = 'ge';

                break;
            case '<':
                $method = 'lt';

                break;
            case '<=':
                $method = 'le';

                break;
        }

        return $this->$method($column, $value, $boolean);
    }

    public function add(string $filter, string $boolean = 'and'): self
    {
        if (0 === count($this->filters) || (count($this->filters) > 0 && '(' === $this->filters[(count($this->filters) - 1)])) {
            $this->filters[] = $filter;

            return $this;
        }

        $this->filters[] = sprintf(
            ' %s %s',
            $boolean,
            $filter
        );

        return $this;
    }
}
