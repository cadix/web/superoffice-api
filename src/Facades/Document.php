<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Document as RootDocument;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootDocument
 *
 * @method static object|null find(int $id)
 * @method static array|null all()
 * @method static array|null get()
 */
class Document extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootDocument::class;
    }
}
