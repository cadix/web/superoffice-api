<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\Business as BusinessMap;

class Business extends Model
{
    protected string $model = 'List/Business/Items';

    /**
     * @return array<BusinessMap>|null
     */
    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();

        $businesses = null;
        foreach ($response as $business) {
            $businesses[] = new BusinessMap($business);
        }

        return $businesses;
    }

    /**
     * @return array<BusinessMap>|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::get();

        $businesses = null;
        foreach ($response as $business) {
            $businesses[] = new BusinessMap($business);
        }

        return $businesses;
    }

    public function find(int $id): BusinessMap|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return new BusinessMap(parent::get());
    }
}
