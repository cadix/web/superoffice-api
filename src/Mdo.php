<?php

namespace Cadix\SuperOfficeApi;

class Mdo extends Model
{
    protected string $model = 'MDOList';

    public function getList(string $listName): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$listName;

        return parent::get();
    }
}
