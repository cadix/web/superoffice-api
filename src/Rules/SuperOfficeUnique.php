<?php

namespace Cadix\SuperOfficeApi\Rules;

use Cadix\SuperOfficeApi\Filter;
use Illuminate\Contracts\Validation\Rule;

class SuperOfficeUnique implements Rule
{
    protected string $attribute;

    public function __construct(
        protected string $model,
        protected string $column,
        protected ?int $except = null
    ) {
    }

    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;

        $filter = (new Filter())->is($this->column, $value);
        if ($this->except) {
            $filter->whereNotIn(lcfirst($this->model::getModel()).'Id', [$this->except]);
        }
        $results = $this->model::filter($filter->get())->get();

        return ! is_countable($results) || count($results) === 0;
    }

    public function message(): string
    {
        return __('validation.unique', [
            'attribute' => $this->attribute,
        ]);
    }
}
