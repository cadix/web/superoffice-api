<?php

namespace Cadix\SuperOfficeApi\Rules;

use Cadix\SuperOfficeApi\Filter;
use Illuminate\Contracts\Validation\Rule;

class SuperOfficeExists implements Rule
{
    protected string $attribute;

    public function __construct(
        protected string $model,
        protected string $column
    ) {
    }

    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;

        $filter = (new Filter())->is($this->column, $value)->get();
        $results = $this->model::filter($filter)->get();

        return is_countable($results) && count($results) > 0;
    }

    public function message(): string
    {
        return __('validation.exists', [
            'attribute' => $this->attribute,
        ]);
    }
}
