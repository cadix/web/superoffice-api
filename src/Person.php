<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\PersonMap;
use Cadix\SuperOfficeApi\Maps\PersonValidatorMap;

class Person extends Model
{
    protected string $model = 'Person';

    /**
     * Gets a ContactEntity object.
     *
     * @param  int         $id
     * @return object|null
     */
    public function find(int $id): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        $map = new PersonMap();
        $response = parent::get();

        return (object) $map((array) $response);
    }

    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();
        $map = new PersonMap();

        return (array) $map($response['value']);
    }

    /**
     * @return array|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;
        $map = new PersonMap();

        $response = parent::get();

        return (array) $map($response['value']);
    }

    /**
     * Store the existing ContactEntity.
     *
     * @param  array  $attributes
     * @return object
     */
    public function create(array $attributes): object
    {
        $map = new PersonValidatorMap();
        $person = (array)$map($attributes);

        if (! $this->validate($person)) {
            throw new Exception('Invalid attributes');
        }

        $this->client->url = parent::getBaseUrl().$this->model;

        return (object)$this->post($person);
    }

    /**
     * Set default values into a new PersonEntity
     * https://community.superoffice.com/documentation/sdk/SO.NetServer.Web.Services/html/v1PersonEntity_DefaultPersonEntity.htm
     *
     * @return object
     */
    public function default(): object
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/default';

        $map = new PersonMap();

        $response = parent::get();

        return (object) $map((array) $response);
    }

    /**
     * Updates the existing ContactEntity.
     *
     * @param  int    $id
     * @param  array  $attributes
     * @return object
     */
    public function update(int $id, array $attributes): object
    {
        $map = new PersonValidatorMap();
        $person = (array)$map($attributes);

        if (! $this->validate($person)) {
            throw new Exception('Invalid attributes');
        }

        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return (object)$this->put($person);
    }

    public function validate(array $person): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/Validate';
        $this->client->params = null; // Reset or filters could be added

        $validated = $this->post($person);

        return is_null($validated) || (is_array($validated) && count($validated) === 0);
    }

    public function delete(int $id): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return parent::destroy($id);
    }
}
