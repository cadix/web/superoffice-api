<?php

namespace Cadix\SuperOfficeApi\Maps;

abstract class Mapper
{
    private array $countries;

    public function __construct()
    {
        $this->countries = require __DIR__.'/../countries/index.php';
    }

    /**
     * @return array|object|null
     */
    public function __invoke(array $toMap): array|object|null
    {
        $maps = null;
        if (0 === count($toMap)) {
            return $maps;
        }

        if (isset($toMap[0])) {
            foreach ($toMap as $map) {
                $maps[] = $this->map((object) $map);
            }
        }

        if (! isset($toMap[0])) {
            $maps = (object) $this->map((object) $toMap);
        }

        return $maps;
    }

    abstract public function map(object $toMap);

    public function countryCode(?string $country): string|null
    {
        if (is_null($country)) {
            return null;
        }

        $return = null;
        if (2 === mb_strlen($country) && is_null($return)) {
            $return = array_search($country, $this->countries, true);
        }

        if (mb_strlen($country) > 2 && is_null($return)) {
            $return = $this->countries[$country];
        }

        return $return;
    }

    public function formatValue($value): string|int|null
    {
        if (empty(trim($value))) {
            return null;
        }

        if (is_numeric($value)) {
            return (int) $value;
        }

        return trim($value);
    }
}
