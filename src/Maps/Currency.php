<?php

namespace Cadix\SuperOfficeApi\Maps;

class Currency
{
    public int         $id;
    public string|null $name;
    public string|null $tooltip;
    public int|null    $rank;
    public float|null  $rate;
    public float|null  $units;
    public bool|null   $deleted;

    public function __construct(array|null $array = null)
    {
        if ($array) {
            $this->fillFromArray((object)$array);
        }
    }

    protected function fillFromArray(object $object): void
    {
        $this->id = $object->id ?? $object->Id ?? $object->CurrencyId ?? null;
        $this->name = $object->Name ?? null;
        $this->tooltip = (! empty($object->Tooltip) ? $object->Tooltip : null) ?? null;
        $this->rank = $object->Rank ?? null;
        $this->rate = $object->Rate ?? null;
        $this->units = $object->Units ?? null;
        $this->deleted = $object->Deleted ?? null;
    }
}
