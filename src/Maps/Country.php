<?php

namespace Cadix\SuperOfficeApi\Maps;

class Country
{
    public int           $id;
    public string|null   $name;
    public int|null      $currency_id;
    public Currency|null $currency;
    public string|null   $english_name;
    public string|null   $two_letter_iso_country;
    public string|null   $three_letter_iso_country;
    public string|null   $image;
    public string|null   $org_nr_text;
    public string|null   $international_area_prefix;
    public string|null   $dial_in_prefix;
    public string|null   $zip_prefix;
    public string|null   $domain_name;
    public int|null      $address_layout_id;
    public int|null      $domestic_address_layout_id;
    public int|null      $foreign_address_layout_id;
    public string|null   $tooltip;
    public int|null      $rank;
    public float|null    $rate;
    public bool|null     $deleted;

    public function __construct(array|null $array = null)
    {
        if ($array) {
            $this->fillFromArray((object)$array);
        }
    }

    protected function fillFromArray(object $object): void
    {
        $this->id = $object->id ?? $object->Id ?? $object->CountryId ?? null;
        $this->name = $object->Name ?? null;
        $this->currency_id = $object->CurrencyId ?? null;
        $this->english_name = $object->EnglishName ?? null;
        $this->two_letter_iso_country = $object->TwoLetterISOCountry ?? null;
        $this->three_letter_iso_country = $object->ThreeLetterISOCountry ?? null;
        $this->image = $object->ImageDescription ?? null;
        $this->org_nr_text = $object->OrgnNrText ?? null;
        $this->international_area_prefix = $object->InterAreaPrefix ?? null;
        $this->dial_in_prefix = $object->DialInPrefix ?? null;
        $this->zip_prefix = $object->ZipPrefix ?? null;
        $this->domain_name = $object->DomainName ?? null;
        $this->address_layout_id = $object->AddressLayoutId ?? null;
        $this->domestic_address_layout_id = $object->DomesticAddressLayoutId ?? null;
        $this->foreign_address_layout_id = $object->ForeingAddressLayoutId ?? null;
        $this->tooltip = (! empty($object->Tooltip) ? $object->Tooltip : null) ?? null;
        $this->rank = $object->Rank ?? null;
        $this->deleted = $object->Deleted ?? null;
    }
}
