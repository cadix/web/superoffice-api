<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\ContactMap;
use Cadix\SuperOfficeApi\Maps\ContactValidatorMap;

/**
 * Class Contact.
 *
 * @property int    $contactId
 * @property string $name
 */
class Contact extends Model
{
    protected string $model = 'Contact';

    /**
     * Gets a ContactEntity object.
     *
     * @param  int         $id
     * @return object|null
     */
    public function find(int $id): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;
        $response = parent::get();

        $map = new ContactMap();

        return (object) $map((array) $response);
    }

    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();
        $map = new ContactMap();

        return (array) $map($response['value']);
    }

    /**
     * @return array|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;
        $response = parent::get();

        $map = new ContactMap();

        return ! $response ? $response : (array) $map($response['value']);
    }

    public function create(array $attributes): object
    {
        $map = new ContactValidatorMap();
        $contact = (array)$map($attributes);

        if (! $this->validate($contact)) {
            throw new Exception('Invalid attributes');
        }

        $this->client->url = parent::getBaseUrl().$this->model;

        return (object)$this->post($contact);
    }

    /**
     * Set default values into a new ContactEntity
     * https://community.superoffice.com/documentation/sdk/SO.NetServer.Web.Services/html/v1ContactEntity_DefaultContactEntity.htm
     *
     * @return object
     */
    public function default(): object
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/default';

        $response = parent::get();

        $map = new ContactMap();

        return (object) $map((array) $response);
    }

    /**
     * Updates the existing ContactEntity.
     *
     * @param  int    $id
     * @param  array  $attributes
     * @return object
     */
    public function update(int $id, array $attributes): object
    {
        $map = new ContactValidatorMap();
        $contact = (array)$map($attributes);

        if (! $this->validate($contact)) {
            throw new Exception('Invalid attributes');
        }

        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return (object)$this->put($contact);
    }

    public function validate(array $contact): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/Validate';
        $this->client->params = null; // Reset or filters could be added

        $validated = $this->post($contact);

        return is_null($validated) || (is_array($validated) && count($validated) === 0);
    }

    public function delete(int $id): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return parent::destroy($id);
    }
}
