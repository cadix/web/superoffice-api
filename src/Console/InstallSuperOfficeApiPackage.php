<?php

namespace Cadix\SuperOfficeApi\Console;

use Illuminate\Console\Command;

class InstallSuperOfficeApiPackage extends Command
{
    protected $signature = 'superoffice-api:install';

    protected $description = 'Install the SuperOffice API package';

    public function handle(): int
    {
        $this->info('Installing SuperOffice API package...');

        $this->info('Publishing configuration...');
        $this->call('vendor:publish', [
            '--provider' => 'Cadix\SuperOfficeApi\SuperOfficeApiServiceProvider',
            '--tag'      => 'config',
        ]);

        $this->info('Publishing migration...');
        $this->call('vendor:publish', [
            '--provider' => 'Cadix\SuperOfficeApi\SuperOfficeApiServiceProvider',
            '--tag'      => 'migrations',
        ]);

        $this->info('SuperOffice API package installed');

        return 1;
    }
}
