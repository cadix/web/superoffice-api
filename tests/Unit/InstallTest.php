<?php

namespace Cadix\SuperOfficeApi\Tests\Unit;

use Cadix\SuperOfficeApi\Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class InstallTest extends TestCase
{
    /**
     * @test
     */
    public function it_will_migrate(): void
    {
        Carbon::setTestNow();
        $configFile = config_path('superoffice.php');
        if (File::exists($configFile)) {
            unlink($configFile);
        }
        $this->assertFalse(File::exists($configFile));

        Artisan::call('superoffice-api:install');
        $migrationFile = database_path('migrations/'.Carbon::now()->format('Y_m_d_His').'_create_superoffice_oauth_table.php');

        $this->assertTrue(File::exists($configFile));
        $this->assertTrue(File::exists($migrationFile));

        if (File::exists($migrationFile)) {
            unlink($migrationFile);
        }
        $this->assertFalse(File::exists($migrationFile));
    }
}
