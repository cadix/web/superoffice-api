<?php

namespace Cadix\SuperOfficeApi\Tests\Unit;

use Cadix\SuperOfficeApi\Filter;
use Cadix\SuperOfficeApi\Tests\TestCase;

class FilterTest extends TestCase
{
    /**
     * @test
     */
    public function method_like_returns_contains(): void
    {
        $filter = (new Filter())->like('name', '%cadix%');

        $this->assertSame($filter->get(), 'name contains '."'cadix'");
    }

    /**
     * @test
     */
    public function method_like_returns_is(): void
    {
        $filter = (new Filter())->like('name', 'cadix');

        $this->assertSame($filter->get(), 'name is '."'cadix'");
    }

    /**
     * @test
     */
    public function method_like_returns_begins(): void
    {
        $filter = (new Filter())->like('name', 'cadix%');

        $this->assertSame($filter->get(), 'name begins '."'cadix'");
    }

    /**
     * @test
     */
    public function method_like_returns_ends(): void
    {
        $filter = (new Filter())->like('name', '%cadix');

        $this->assertSame($filter->get(), 'name ends '."'cadix'");
    }

    /**
     * @test
     */
    public function begins(): void
    {
        $filter = (new Filter())->begins('name', 'cadix');

        $this->assertSame($filter->get(), 'name begins '."'cadix'");
    }

    /**
     * @test
     */
    public function ends(): void
    {
        $filter = (new Filter())->ends('name', 'cadix');

        $this->assertSame($filter->get(), 'name ends '."'cadix'");
    }

    /**
     * @test
     */
    public function contains(): void
    {
        $filter = (new Filter())->contains('name', 'cadix');

        $this->assertSame($filter->get(), 'name contains '."'cadix'");
    }

    /**
     * @test
     */
    public function is(): void
    {
        $filter = (new Filter())->is('name', 'cadix');

        $this->assertSame($filter->get(), "name is 'cadix'");
    }

    /**
     * @test
     */
    public function where_returns_is(): void
    {
        $filter = (new Filter())->where('name', 'cadix');

        $this->assertSame($filter->get(), "name is 'cadix'");
    }

    /**
     * @test
     */
    public function is_returns_equals_when_integer_provided(): void
    {
        $filter = (new Filter())->is('contactId', '3');

        $this->assertSame($filter->get(), 'contactId equals 3');
    }

    /**
     * @test
     */
    public function bool(): void
    {
        $filter = (new Filter())->bool('hasContact', true);

        $this->assertSame($filter->get(), 'hasContact = 1');
    }

    /**
     * @test
     */
    public function equals(): void
    {
        $filter = (new Filter())->equals('contactId', 2);

        $this->assertSame($filter->get(), 'contactId equals 2');
    }

    /**
     * @test
     */
    public function where_returns_equals(): void
    {
        $filter = (new Filter())->where('contactId', 2);

        $this->assertSame($filter->get(), "contactId equals 2");
    }

    /**
     * @test
     */
    public function gt(): void
    {
        $filter = (new Filter())->gt('contactId', 2);

        $this->assertSame($filter->get(), 'contactId gt 2');
    }

    /**
     * @test
     */
    public function where_returns_gt(): void
    {
        $filter = (new Filter())->where('contactId', 2, '>');

        $this->assertSame($filter->get(), 'contactId gt 2');
    }

    /**
     * @test
     */
    public function greater_than_returns_gt(): void
    {
        $filter = (new Filter())->greaterThan('contactId', 2);

        $this->assertSame($filter->get(), 'contactId gt 2');
    }

    /**
     * @test
     */
    public function ge(): void
    {
        $filter = (new Filter())->ge('contactId', 2);

        $this->assertSame($filter->get(), 'contactId ge 2');
    }

    /**
     * @test
     */
    public function where_returns_ge(): void
    {
        $filter = (new Filter())->where('contactId', 2, '>=');

        $this->assertSame($filter->get(), 'contactId ge 2');
    }

    /**
     * @test
     */
    public function greater_than_or_equals_returns_ge(): void
    {
        $filter = (new Filter())->greaterThanOrEquals('contactId', 2);

        $this->assertSame($filter->get(), 'contactId ge 2');
    }

    /**
     * @test
     */
    public function lt(): void
    {
        $filter = (new Filter())->lt('contactId', 2);

        $this->assertSame($filter->get(), 'contactId lt 2');
    }

    /**
     * @test
     */
    public function where_returns_lt(): void
    {
        $filter = (new Filter())->where('contactId', 2, '<');

        $this->assertSame($filter->get(), 'contactId lt 2');
    }

    /**
     * @test
     */
    public function less_than_returns_lt(): void
    {
        $filter = (new Filter())->lessThan('contactId', 2);

        $this->assertSame($filter->get(), 'contactId lt 2');
    }

    /**
     * @test
     */
    public function le(): void
    {
        $filter = (new Filter())->le('contactId', 2);

        $this->assertSame($filter->get(), 'contactId le 2');
    }

    /**
     * @test
     */
    public function where_returns_le(): void
    {
        $filter = (new Filter())->where('contactId',  2, '<=');

        $this->assertSame($filter->get(), 'contactId le 2');
    }

    /**
     * @test
     */
    public function less_than_or_equals_returns_le(): void
    {
        $filter = (new Filter())->lessThanOrEquals('contactId', 2);

        $this->assertSame($filter->get(), 'contactId le 2');
    }

    /**
     * @test
     */
    public function where_in(): void
    {
        $filter = (new Filter())->whereIn('contactId', [2, 4]);

        $this->assertSame($filter->get(), 'contactId in(2,4)');
    }

    /**
     * @test
     */
    public function where_not_in(): void
    {
        $filter = (new Filter())->whereNotIn('contactId', [2, 4]);

        $this->assertSame($filter->get(), 'contactId notOneOf(2,4)');
    }

    /**
     * @test
     */
    public function by_default_filters_separated_by_and(): void
    {
        $filter = (new Filter())
            ->whereNotIn('contactId', [2, 4])
            ->whereIn('contactId', [2, 4]);

        $this->assertSame($filter->get(), 'contactId notOneOf(2,4) and contactId in(2,4)');
    }

    /**
     * @test
     */
    public function filters_can_separated_by_or(): void
    {
        $filter = (new Filter())
            ->whereNotIn('contactId', [2, 4])
            ->whereIn('contactId', [2, 4], 'or');

        $this->assertSame($filter->get(), 'contactId notOneOf(2,4) or contactId in(2,4)');
    }
}
