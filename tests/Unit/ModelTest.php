<?php

namespace Cadix\SuperOfficeApi\Tests\Unit;

use Cadix\SuperOfficeApi\Facades\Contact;
use Cadix\SuperOfficeApi\Filter;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ModelTest extends TestCase
{
    public $baseUrl;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function __construct()
    {
        parent::__construct();

        $this->baseUrl = sprintf(
            'https://%s.superoffice.com/%s/api/v1/%s',
            $_ENV['SUPEROFFICE_ENVIRONMENT'],
            $_ENV['SUPEROFFICE_CUSTOMER_ID'],
            'Contact'
        );
    }

    /**
     * @test
     */
    public function it_will_add_separate_params(): void
    {
        $call = Contact::select(['ContactId'])->top(10);

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$select', $params);
        $this->assertArrayHasKey('$top', $params);
        $this->assertSame($params['$select'], 'ContactId');
        $this->assertSame($params['$top'], 10);
    }

    /**
     * @test
     */
    public function when_using_select_it_will_turn_array_into_string(): void
    {
        $call = Contact::select(['ContactId', 'Name']);

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$select', $params);
        $this->assertSame($params['$select'], 'ContactId,Name');
    }

    /**
     * @test
     */
    public function it_can_paginate_results(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $contacts = file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json');

        Http::fake([
            '*' => Http::sequence()
                ->push($contacts, 200) // Get total amount of contacts
                ->push($contacts, 200), // Get paginated result
        ]);

        $contacts = Contact::paginate();

        $this->assertIsObject($contacts);

        $this->assertSame($contacts->total, 123);
        $this->assertSame($contacts->per_page, 10);
        $this->assertSame($contacts->current_page, 1);
        $this->assertSame($contacts->last_page, 13);
        $this->assertSame($contacts->from, 1);
        $this->assertSame($contacts->to, 10);
        $this->assertIsArray($contacts->data);
    }

    /**
     * @test
     */
    public function paginate_results_on_higher_page(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $contacts = file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json');

        Http::fake([
            '*' => Http::sequence()
                ->push($contacts, 200) // Get total amount of contacts
                ->push($contacts, 200), // Get paginated result
        ]);

        $contacts = Contact::paginate(10, 2);

        $this->assertIsObject($contacts);

        $this->assertSame($contacts->total, 123);
        $this->assertSame($contacts->per_page, 10);
        $this->assertSame($contacts->current_page, 2);
        $this->assertSame($contacts->last_page, 13);
        $this->assertSame($contacts->from, 20);
        $this->assertSame($contacts->to, 30);
        $this->assertIsArray($contacts->data);
    }

    /**
     * @test
     */
    public function models_can_be_ordered_by_string(): void
    {
        $call = Contact::orderBy('name asc');

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$orderBy', $params);
        $this->assertSame($params['$orderBy'], 'name asc');
    }

    /**
     * @test
     */
    public function models_can_be_ordered_by_array(): void
    {
        $call = Contact::orderBy(['name' => 'asc']);

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$orderBy', $params);
        $this->assertSame($params['$orderBy'], 'name asc');
    }

    /**
     * @test
     */
    public function models_can_be_ordered_by_multiple_array_items(): void
    {
        $call = Contact::orderBy(['name' => 'asc', 'contact_id' => 'desc']);

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$orderBy', $params);
        $this->assertSame($params['$orderBy'], 'name asc,contact_id desc');
    }

    /**
     * @test
     */
    public function limit_returns_top(): void
    {
        $call = Contact::limit(20);

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$top', $params);
        $this->assertSame($params['$top'], 20);
    }

    /**
     * @test
     */
    public function it_can_skip_results(): void
    {
        $call = Contact::skip(30);

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$skip', $params);
        $this->assertSame($params['$skip'], 30);
    }

    /**
     * @test
     */
    public function it_adds_filters(): void
    {
        $filter = ( new Filter() )->is('name', 'cadix');
        $call = Contact::filter($filter->get());

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$filter', $params);
        $this->assertSame($params['$filter'], "name is 'cadix'");
    }

    /**
     * @test
     */
    public function it_adds_entities(): void
    {
        $call = Contact::entities('"contact". "contact, person"');

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$entities', $params);
        $this->assertSame($params['$entities'], '"contact". "contact, person"');
    }

    /**
     * @test
     */
    public function it_adds_mode(): void
    {
        $call = Contact::mode('SLIM');

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$mode', $params);
        $this->assertSame($params['$mode'], 'SLIM');
    }

    /**
     * @test
     */
    public function it_adds_options(): void
    {
        $call = Contact::options('GrandTotal=true');

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$options', $params);
        $this->assertSame($params['$options'], 'GrandTotal=true');
    }

    /**
     * @test
     */
    public function it_adds_context(): void
    {
        $call = Contact::context('ab');

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$context', $params);
        $this->assertSame($params['$context'], 'ab');
    }

    /**
     * @test
     */
    public function it_adds_format(): void
    {
        $call = Contact::format('xml');

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$format', $params);
        $this->assertSame($params['$format'], 'xml');
    }

    /**
     * @test
     */
    public function it_adds_json_safe(): void
    {
        $call = Contact::jsonSafe(true);

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$jsonSafe', $params);
        $this->assertSame($params['$jsonSafe'], true);
    }

    /**
     * @test
     */
    public function it_adds_output(): void
    {
        $call = Contact::output('Logical');

        $params = $call->getParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey('$output', $params);
        $this->assertSame($params['$output'], 'Logical');
    }
}
