<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Client;
use Cadix\SuperOfficeApi\Facades\Auth;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Session;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Cadix\SuperOfficeApi\Tests\User;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth as LaravelAuth;
use Illuminate\Support\Facades\Http;

class AuthenticateTest extends TestCase
{
    /**
     * @test
     */
    public function it_redirects_to_superoffice_on_login(): void
    {
        $this->followRedirects(Auth::login())->assertSee('.antialiased');
    }

    /**
     * @test
     */
    public function it_redirects_back_if_already_logged_in(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        LaravelAuth::login($oauth->user);

        $this->assertAuthenticatedAs($oauth->user);

        $this->actingAs($oauth->user)->followRedirects(Auth::login())->assertSee('.antialiased');
    }

    /**
     * @test
     */
    public function on_login_callback_it_can_request_an_access_token(): void
    {
        $content = file_get_contents(__DIR__.'/../_sample-responses/login_tokens.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $code = '4959ff034jf043';
        $response = Auth::requestAccessToken($code);

        Http::assertSent(function (Request $request) use ($code) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/login/common/oauth/tokens?client_id=%s&client_secret=%s&code=%s&redirect_uri=%s&grant_type=authorization_code',
                config('superoffice.environment'),
                config('superoffice.client_id'),
                config('superoffice.client_secret'),
                $code,
                config('superoffice.redirect_url')
            ) &&
                $request->method() === 'POST';
        });

        $content = json_decode($content);

        $this->assertSame($response->id_token, $content->id_token);
        $this->assertSame($response->access_token, $content->access_token);
        $this->assertSame($response->refresh_token, $content->refresh_token);
    }

    /**
     * @test
     */
    public function on_login_redirect_it_stores_tokens(): void
    {
        $model = User::factory()->create();

        $url = sprintf('https://%s.superoffice.com', config('superoffice.environment'));
        $content = file_get_contents(__DIR__.'/../_sample-responses/login_tokens.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $client = app(Client::class);
        $client->url = $url;

        $user = (object) $client->get();
        Http::assertSent(function (Request $request) use ($url) {
            return $request->url() === $url && $request->method() === 'GET';
        });

        ( new \Cadix\SuperOfficeApi\Session() )->setTokens($model, $user->access_token, $user->refresh_token, $user->id ?? null);

        $this->assertDatabaseHas('superoffice_oauth', [
            'user_type'     => get_class($model),
            'user_id'       => $model->id,
            'access_token'  => $user->access_token,
            'refresh_token' => $user->refresh_token,
        ]);
    }

    /**
     * @test
     */
    public function if_no_refresh_token_is_provided_it_redirects_to_login(): void
    {
        $session = ( new Session() );

        $this->followRedirects($session->getRefreshToken())->assertSee('.antialiased');

        $token = 'tester';
        $session->setRefreshToken($token);

        $response = $session->getRefreshToken();

        $this->assertTrue($response === $token);
    }

    /**
     * @test
     */
    public function check_returns_true_if_the_user_is_logged_in(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        \Illuminate\Support\Facades\Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/User/current.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $this->assertTrue(Auth::check());

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).'User/currentPrincipal' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function check_returns_false_if_the_user_is_not_logged_in(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        \Illuminate\Support\Facades\Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $this->assertFalse(Auth::check());

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).'User/currentPrincipal' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function check_returns_false_if_the_response_is_an_error(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        \Illuminate\Support\Facades\Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/User/current.json');

        Http::fake([
            '*' => Http::sequence()
                ->push($content, 400, ['Content-Type' => 'application/json'])
                ->push($content, 400, ['Content-Type' => 'application/json']),
        ]);

        $this->assertFalse(Auth::check());
    }
}
