<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\Client;
use Cadix\SuperOfficeApi\Facades\Contact;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ClientTest extends TestCase
{
    /**
     * @test
     */
    public function it_sets_request_headers(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $contact = Contact::select(['contactId', 'name', 'department', 'orgnr', 'number', 'contactAssociate/personEmail', 'postAddress/city', 'postAddress/zip', 'postAddress/line1', 'country'])
            ->orderBy(['name' => 'asc'])
            ->limit(20);

        $headers = $contact->getHeaders();
        $this->assertIsArray($headers);
        $this->assertSame($headers, $contact->client->headers);
    }

    /**
     * @test
     */
    public function a_client_can_set_credentials(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $this->assertAuthenticatedAs($oauth->user);

        $token = $oauth->access_token;
        $refresh_token = $oauth->refresh_token;
        $user_id = $oauth->user_id;

        $client = Client::setTokens($oauth->user, $token, $refresh_token, $user_id);

        $this->assertSame($token, Client::getAccessToken());
        $this->assertSame($refresh_token, Client::getRefreshToken());
    }
}
