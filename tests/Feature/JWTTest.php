<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Tests\TestCase;
use Cadix\SuperOfficeApi\Validators\JWTValidator;
use DateTimeImmutable;
use Lcobucci\JWT\Configuration;

class JWTTest extends TestCase
{
    /**
     * @test
     */
    public function validate_bad_token(): void
    {
        $token_id = '234rfoifsjf';

        $this->expectException('\Lcobucci\JWT\Token\InvalidTokenStructure');
        $this->expectExceptionMessage('The JWT string must have two dots');

        (new \Cadix\SuperOfficeApi\Validators\JWTValidator())->validateAndVerifyJwt($token_id);
    }

    /**
     * @test
     */
    public function validate_valid_token(): void
    {
        $config = (new JWTValidator())->configuration;
        assert($config instanceof Configuration);

        $now = new DateTimeImmutable();

        $token = $config->builder()
            ->issuedAt($now)
            ->expiresAt($now->modify('+1 hour'))
            ->issuedBy(sprintf('https://%s.superoffice.com', config('superoffice.environment')))
            ->getToken($config->signer(), $config->signingKey());

        $result = (new JWTValidator())->validateAndVerifyJwt($token->toString());

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function validate_expired_token_with_valid_issuer(): void
    {
        $config = (new JWTValidator())->configuration;
        assert($config instanceof Configuration);

        $now = new DateTimeImmutable();

        $token = $config->builder()
            ->issuedAt($now->sub(new \DateInterval('PT3000S')))
            ->expiresAt($now->sub(new \DateInterval('PT3600S')))
            ->issuedBy(sprintf('https://%s.superoffice.com', config('superoffice.environment')))
            ->getToken($config->signer(), $config->signingKey());

        $this->expectException('\Lcobucci\JWT\Validation\RequiredConstraintsViolated');
        $this->expectExceptionMessage('The token is expired');

        (new JWTValidator())->validateAndVerifyJwt($token->toString());
    }

    /**
     * @test
     */
    public function validate_token_with_invalid_issuer(): void
    {
        $config = (new JWTValidator())->configuration;
        assert($config instanceof Configuration);

        $now = new DateTimeImmutable();

        $token = $config->builder()
            ->issuedAt($now)
            ->expiresAt($now->add(new \DateInterval('PT3600S')))
            ->issuedBy(sprintf('https://%s.superoffice.com', 'invalid-issuer'))
            ->getToken($config->signer(), $config->signingKey());

        $this->expectException('\Lcobucci\JWT\Validation\RequiredConstraintsViolated');
        $this->expectExceptionMessage('The token was not issued by the given issuers');

        (new JWTValidator())->validateAndVerifyJwt($token->toString());
    }
}
