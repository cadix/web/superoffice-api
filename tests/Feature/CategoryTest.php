<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\Category;
use Cadix\SuperOfficeApi\Maps\Category as CategoryMap;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class CategoryTest extends TestCase
{
    private string $model = 'List/Category/Items';

    /**
     * @test
     */
    public function it_can_get_all(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $content = file_get_contents(__DIR__ . '/../_sample-responses/Category/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $response = Category::all();

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer ' . $oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });

        $this->assertIsArray($response);
        $this->assertInstanceOf(CategoryMap::class, $response[ 0 ]);
    }

    /**
     * @test
     */
    public function it_can_use_get(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $content = file_get_contents(__DIR__ . '/../_sample-responses/Category/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $categoryes = Category::get();

        $this->assertIsArray($categoryes);
        $this->assertInstanceOf(CategoryMap::class, $categoryes[ 0 ]);

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer ' . $oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function it_can_use_find(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__ . '/../_sample-responses/Category/find.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 2;
        $category = Category::find($id);
        $this->assertIsObject($category);
        $this->assertInstanceOf(CategoryMap::class, $category);
        $this->assertEquals($id, $category->id);

        Http::assertSent(function (Request $request) use ($oauth, $id) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model . '/' . $id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer ' . $oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }
}
