<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\User;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class UserTest extends TestCase
{
    private string $model = 'User';

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_get_all(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/User/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $this->assertIsArray(User::all());

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_use_get(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/User/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $this->assertIsArray(User::get());

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_get_current_user(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/User/current.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $response = User::itsAMeMario();
        $this->assertIsObject($response);
        $this->assertTrue(159 === $response->person_id);

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model.'/currentPrincipal' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }
}
