<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\Contact;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ContactTest extends TestCase
{
    private string $model = 'Contact';

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_get_all(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $contacts = Contact::all();

        $this->assertIsArray($contacts);

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_use_get(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $contact = Contact::get();

        $this->assertIsArray($contact);

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_find_by_id(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/Contact/find.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 3;
        $contact = Contact::find($id);

        $this->assertIsObject($contact);
        $this->assertEquals($id, $contact->contact_id);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model.'/'.$id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function it_can_create_a_contact(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $contact = Contact::create([
            'name'       => 'Vitens',
            'department' => 'Zwolle',
            'country'    => 'Netherlands',
            'person_id'  => 2,
            'address'    => [
                'postal' => [
                    'line1' => 'Stationstraat 2',
                    'zip'   => '3443AT',
                    'city'  => 'Zwolle',
                ],
            ],
            'phone' => 12345678990,
        ]);

        Http::assertSent(function (Request $request) use ($oauth) {
            return (
                $request->url() === sprintf(
                    'https://%s.superoffice.com/%s/api/v1/',
                    config('superoffice.environment'),
                    config('superoffice.customer_id')
                ).$this->model.'/Validate' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'POST'
                )
                ||
                (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model &&
                    $request->hasHeaders([
                        'Bearer',
                        'Accept',
                        'Content-Type',
                    ]) &&
                    $request->method() === 'POST'
                );
        });


        $this->assertTrue(is_object($contact));
    }

    /**
     * @test
     */
    public function it_can_update_a_contact(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response([], 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 1;
        $contact = Contact::update($id, [
            'contact_id'   => 1,
            'name'         => 'Vitens',
            'department'   => 'Zwolle',
            'country'      => 'Netherlands',
            'associate_id' => 20,
            'business_id'  => 12,
            'category_id'  => 9,
            'address'      => [
                'postal' => [
                    'line1' => 'Stationstraat 2',
                    'zip'   => '3443AT',
                    'city'  => 'Zwolle',
                ],
            ],
        ]);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model.'/Validate' &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'POST'
                )
                ||
                (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model.'/'.$id &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'PUT'
                );
        });

        $this->assertIsObject($contact);
    }

    /**
     * @test
     */
    public function it_can_delete_a_contact(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 10;
        $deleted = Contact::delete($id);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model.'/'.$id &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'DELETE';
        });

        $this->assertTrue($deleted);
    }

    /**
     * @test
     */
    public function it_can_create_a_new_default_contact(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(file_get_contents(__DIR__.'/../_sample-responses/Contact/default.json'), 200, ['Content-Type' => 'application/json']),
        ]);

        $default = Contact::default();

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model.'/default' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });

        $this->assertIsObject($default);
        $this->assertObjectHasAttribute('contact_id', $default);
    }
}
