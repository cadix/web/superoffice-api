<?php

namespace Cadix\SuperOfficeApi\Tests;

use Cadix\SuperOfficeApi\Client;
use Cadix\SuperOfficeApi\SuperOfficeApiServiceProvider;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
use Spatie\GuzzleRateLimiterMiddleware\InMemoryStore;
use Spatie\GuzzleRateLimiterMiddleware\RateLimiter;

class TestCase extends \Orchestra\Testbench\TestCase
{
    /** @var \Cadix\SuperOfficeApi\Tests\TestDeferrer */
    protected $deferrer;

    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app): array
    {
        return [
            SuperOfficeApiServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app): void
    {
        // make sure, our .env file is loaded
        $app->useEnvironmentPath(__DIR__.'/..');
        $app->bootstrapWith([LoadEnvironmentVariables::class]);
        parent::getEnvironmentSetUp($app);
        $app['config']->set('superoffice.environment', env('SUPEROFFICE_ENVIRONMENT'));
        $app['config']->set('superoffice.customer_id', env('SUPEROFFICE_CUSTOMER_ID'));
        $app['config']->set('superoffice.client_id', env('SUPEROFFICE_CLIENT_ID'));
        $app['config']->set('superoffice.client_secret', env('SUPEROFFICE_CLIENT_SECRET'));
        $app['config']->set('superoffice.redirect_url', env('SUPEROFFICE_REDIRECT_URL'));
        $app['config']->set('superoffice.retires', env('SUPEROFFICE_RETRIES'));
        $app['config']->set('superoffice.timeout', env('SUPEROFFICE_TIMEOUT'));

        // import the CreateSuperofficeOauthTable class from the migration
        include_once __DIR__.'/../database/migrations/create_superoffice_oauth_table.php';
        include_once __DIR__.'/../database/migrations/create_users_table.php';

        // run the up() method of that migration class
        ( new \CreateSuperofficeOauthTable() )->up();
        ( new \CreateUsersTable() )->up();
    }

    /**
     * @param string|array $content
     */
    public function createMockResponse($content = null, int $status = 200): Response
    {
        $response = new Response($status, ['Content-Type' => 'application/json'], $content);

        $mock = new MockHandler([$response]);

        $handler = HandlerStack::create($mock);
        $this->client = new Client(['handler' => $handler]);

        $this->app->instance(Client::class, $this->client);

        return $response;
    }

    public function createRateLimiter(int $limit, string $timeFrame): RateLimiter
    {
        $this->deferrer = new TestDeferrer();

        return new RateLimiter($limit, $timeFrame, new InMemoryStore(), $this->deferrer);
    }
}
