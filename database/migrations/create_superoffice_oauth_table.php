<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperofficeOauthTable extends Migration
{
    public function up(): void
    {
        Schema::create('superoffice_oauth', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('access_token');
            $table->text('refresh_token');
            $table->unsignedBigInteger('superoffice_user_id')->unique()->nullable();
            $table->unsignedBigInteger('user_id');
            $table->string('user_type');
            $table->timestamps();

            $table->unique(['user_id', 'user_type', 'superoffice_user_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('superoffice_oauth');
    }
}
